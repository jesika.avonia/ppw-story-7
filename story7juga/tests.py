from django.test import TestCase, Client
from django.urls import resolve, reverse
from django.apps import apps
from . import views
from . import apps

class UnitTest_Story7(TestCase) :
    
    def setUp(self):
        self.client = Client()
        self.response = self.client.get('')
        self.page_content = self.response.content.decode('utf8')

    def test_story7_url_exists(self):
        self.assertEqual(self.response.status_code, 200)

    def test_story7_template_used(self):
        self.assertTemplateUsed(self.response, 'index.html')


